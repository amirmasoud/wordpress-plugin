<?php
/**
 * The Template for displaying all single posts.
 *
 * @package unite child
 */

get_header(); ?>

    <div id="primary" class="content-area col-sm-12 col-md-8 <?php echo of_get_option( 'site_layout' ); ?>">
        <main id="main" class="site-main" role="main">

        <?php while ( have_posts() ) : the_post(); ?>


            <?php get_template_part( 'content', 'single' ); ?>

            <ul><?php echo get_the_term_list( $post->ID, 'country', '<li class="genres_item">', ', ', '</li>' ) ?></ul>
            <ul><?php echo get_the_term_list( $post->ID, 'genre', '<li class="genres_item">', ', ', '</li>' ) ?></ul>

            <p><?php echo __('Price: ', 'unite-child') . unite_child_get_films_price($post->ID); ?></p>
            <p><?php echo __('Release date: ', 'unite-child') . unite_child_get_films_release_date($post->ID); ?></p>

            <?php unite_post_nav(); ?>

            <?php
                // If comments are open or we have at least one comment, load up the comment template
                if ( comments_open() || '0' != get_comments_number() ) :
                    comments_template();
                endif;
            ?>

        <?php endwhile; // end of the loop. ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
