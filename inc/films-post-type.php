<?php

add_action( 'init', 'unite_child_register_films_custom_post_type' );

/**
 * A new films custom post type with help of Custom Post Type UI
 * plugin. (https://wordpress.org/plugins/custom-post-type-ui/)
 *
 * @return void
 */
function unite_child_register_films_custom_post_type() {

    /**
     * Post Type: Films.
     */

    $labels = array(
        'name' => __( 'Films', 'unite-child' ),
        'singular_name' => __( 'Film', 'unite-child' ),
        'menu_name' => __( 'Films', 'unite-child' ),
        'all_items' => __( 'All Films', 'unite-child' ),
        'add_new' => __( 'Add New', 'unite-child' ),
        'add_new_item' => __( 'Add New Film', 'unite-child' ),
        'edit_item' => __( 'Edit Film', 'unite-child' ),
        'new_item' => __( 'New Film', 'unite-child' ),
        'view_item' => __( 'View Film', 'unite-child' ),
        'view_items' => __( 'View Films', 'unite-child' ),
        'search_items' => __( 'Search Films', 'unite-child' ),
        'not_found' => __( 'No Films Found', 'unite-child' ),
        'not_found_in_trash' => __( 'No Films Found in Trash', 'unite-child' ),
        'parent_item_colon' => __( 'Parent Film:', 'unite-child' ),
        'featured_image' => __( 'Featured Image', 'unite-child' ),
        'set_featured_image' => __( 'Set featured image', 'unite-child' ),
        'remove_featured_image' => __( 'Remove featured image', 'unite-child' ),
        'use_featured_image' => __( 'Use as featured image', 'unite-child' ),
        'archives' => __( 'Film Archives', 'unite-child' ),
        'insert_into_item' => __( 'Insert into Film', 'unite-child' ),
        'uploaded_to_this_item' => __( 'Upload to this Film', 'unite-child' ),
        'filter_items_list' => __( 'Filter films list', 'unite-child' ),
        'items_list_navigation' => __( 'Film list navigation', 'unite-child' ),
        'items_list' => __( 'Films list', 'unite-child' ),
        'parent_item_colon' => __( 'Parent Film:', 'unite-child' ),
    );

    $args = array(
        'label' => __( 'Films', 'unite-child' ),
        'labels' => $labels,
        'description' => 'Add, edit and show films.',
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_rest' => false,
        'rest_base' => '',
        'has_archive' => false,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'exclude_from_search' => false,
        'capability_type' => 'post',
        'map_meta_cap' => true,
        'hierarchical' => false,
        'rewrite' => array( 'slug' => 'films', 'with_front' => true ),
        'query_var' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-format-video',
        'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'comments', 'author' ),
    );

    register_post_type( 'films', $args );
}

/**
 * Add custom field for films post type contains Ticket Price with $ prefix and
 * Release Date with date picker using Advanced Custom Fields plugin. The has
 * been required using tgm plugin.
 * (https://wordpress.org/plugins/advanced-custom-fields/)
 */
if( function_exists('acf_add_local_field_group') ) {
    acf_add_local_field_group(array(
        'key' => 'group_5ba128b3b181e',
        'title' => 'Films',
        'fields' => array(
            array(
                'key' => 'field_5ba128b9259d7',
                'label' => 'Ticket Price',
                'name' => 'ticket_price',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 0,
                'placeholder' => '',
                'prepend' => '$',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5ba128fb259d8',
                'label' => 'Release Date',
                'name' => 'release_date',
                'type' => 'date_picker',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'display_format' => 'd/m/Y',
                'return_format' => 'd/m/Y',
                'first_day' => 1,
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'films',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
}
