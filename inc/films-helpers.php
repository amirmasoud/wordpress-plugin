<?php

/**
 * Get a films price tag, free or a number.
 *
 * @param  int $post_id
 * @return string|float|int
 */
function unite_child_get_films_price($post_id) {
    $unite_child_price = get_post_meta($post_id, 'ticket_price', true);

    return $unite_child_price == 0
        ? __('Free', 'unite-child')
        : __('$', 'unite-child') . $unite_child_price;
}

/**
 * Get a film release date formatted in default date format set in settings.
 *
 * @param  int $post_id
 * @return string
 */
function unite_child_get_films_release_date($post_id) {
    return date(get_option( 'date_format' ), get_post_meta($post->ID, 'release_date', true));
}
