<?php

add_action( 'init', 'unite_child_register_genres_taxonomy' );

/**
 * A new genre taxonomy to films post type with help of Custom Post Type UI
 * plugin. (https://wordpress.org/plugins/custom-post-type-ui/)
 *
 * @return void
 */
function unite_child_register_genres_taxonomy() {

    /**
     * Taxonomy: Genres.
     */
    $labels = array(
        'name' => __( 'Genres', 'unite-child' ),
        'singular_name' => __( 'Genre', 'unite-child' ),
        'menu_name' => __( 'Genre', 'unite-child' ),
        'all_items' => __( 'Genres', 'unite-child' ),
        'edit_item' => __( 'Edit Genre', 'unite-child' ),
        'view_item' => __( 'View Genre', 'unite-child' ),
        'update_item' => __( 'Update Genre Name', 'unite-child' ),
        'add_new_item' => __( 'Add New Genre', 'unite-child' ),
        'new_item_name' => __( 'New Genre Name', 'unite-child' ),
        'parent_item' => __( 'Parent Genre', 'unite-child' ),
        'parent_item_colon' => __( 'Parent Genre:', 'unite-child' ),
        'search_items' => __( 'Search Genres', 'unite-child' ),
        'popular_items' => __( 'Popular Genre', 'unite-child' ),
        'separate_items_with_commas' => __( 'Separate Genres with Commas', 'unite-child' ),
        'add_or_remove_items' => __( 'Add or remove Genres', 'unite-child' ),
        'choose_from_most_used' => __( 'Choose from most used Genres', 'unite-child' ),
        'not_found' => __( 'No Genres Found', 'unite-child' ),
        'no_terms' => __( 'No genres', 'unite-child' ),
        'items_list_navigation' => __( 'Genres list navigation', 'unite-child' ),
        'items_list' => __( 'Genres list', 'unite-child' ),
    );

    $args = array(
        'label' => __( 'Genres', 'unite-child' ),
        'labels' => $labels,
        'public' => true,
        'hierarchical' => false,
        'label' => 'Genres',
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'genre', 'with_front' => true, ),
        'show_admin_column' => false,
        'show_in_rest' => false,
        'rest_base' => 'genre',
        'show_in_quick_edit' => false,
    );
    register_taxonomy( 'genre', array( 'films' ), $args );
}

add_action( 'init', 'unite_child_register_countries_taxonomy' );

/**
 * A new country taxonomy to films post type with help of Custom Post Type UI
 * plugin. (https://wordpress.org/plugins/custom-post-type-ui/)
 *
 * @return void
 */
function unite_child_register_countries_taxonomy() {

    /**
     * Taxonomy: Countries.
     */
    $labels = array(
        'name' => __( 'Countries', 'unite-child' ),
        'singular_name' => __( 'Country', 'unite-child' ),
        'menu_name' => __( 'Country', 'unite-child' ),
        'all_items' => __( 'Countries', 'unite-child' ),
        'edit_item' => __( 'Edit Country', 'unite-child' ),
        'view_item' => __( 'View Country', 'unite-child' ),
        'update_item' => __( 'Update Country Name', 'unite-child' ),
        'add_new_item' => __( 'Add New Country', 'unite-child' ),
        'new_item_name' => __( 'New Country Name', 'unite-child' ),
        'parent_item' => __( 'Parent Country', 'unite-child' ),
        'parent_item_colon' => __( 'Parent Country:', 'unite-child' ),
        'search_items' => __( 'Search Countries', 'unite-child' ),
        'popular_items' => __( 'Popular Country', 'unite-child' ),
        'separate_items_with_commas' => __( 'Separate Countries with Commas', 'unite-child' ),
        'add_or_remove_items' => __( 'Add or remove Countries', 'unite-child' ),
        'choose_from_most_used' => __( 'Choose from most used Countries', 'unite-child' ),
        'not_found' => __( 'No Countries Found', 'unite-child' ),
        'no_terms' => __( 'No countries', 'unite-child' ),
        'items_list_navigation' => __( 'Countries list navigation', 'unite-child' ),
        'items_list' => __( 'Countries list', 'unite-child' ),
    );

    $args = array(
        'label' => __( 'Countries', 'unite-child' ),
        'labels' => $labels,
        'public' => true,
        'hierarchical' => false,
        'label' => 'Countries',
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'country', 'with_front' => true, ),
        'show_admin_column' => false,
        'show_in_rest' => false,
        'rest_base' => 'country',
        'show_in_quick_edit' => false,
    );
    register_taxonomy( 'country', array( 'films' ), $args );
}

add_action( 'init', 'unite_child_register_years_taxonomy' );

/**
 * A new years taxonomy to films post type with help of Custom Post Type UI
 * plugin. (https://wordpress.org/plugins/custom-post-type-ui/)
 *
 * @return void
 */
function unite_child_register_years_taxonomy() {

    /**
     * Taxonomy: Years.
     */
    $labels = array(
        'name' => __( 'Years', 'unite-child' ),
        'singular_name' => __( 'Year', 'unite-child' ),
        'menu_name' => __( 'Year', 'unite-child' ),
        'all_items' => __( 'Years', 'unite-child' ),
        'edit_item' => __( 'Edit Year', 'unite-child' ),
        'view_item' => __( 'View Year', 'unite-child' ),
        'update_item' => __( 'Update Year Name', 'unite-child' ),
        'add_new_item' => __( 'Add New Year', 'unite-child' ),
        'new_item_name' => __( 'New Year Name', 'unite-child' ),
        'parent_item' => __( 'Parent Year', 'unite-child' ),
        'parent_item_colon' => __( 'Parent Year:', 'unite-child' ),
        'search_items' => __( 'Search Years', 'unite-child' ),
        'popular_items' => __( 'Popular Year', 'unite-child' ),
        'separate_items_with_commas' => __( 'Separate Years with Commas', 'unite-child' ),
        'add_or_remove_items' => __( 'Add or remove Years', 'unite-child' ),
        'choose_from_most_used' => __( 'Choose from most used Years', 'unite-child' ),
        'not_found' => __( 'No Years Found', 'unite-child' ),
        'no_terms' => __( 'No years', 'unite-child' ),
        'items_list_navigation' => __( 'Years list navigation', 'unite-child' ),
        'items_list' => __( 'Years list', 'unite-child' ),
    );

    $args = array(
        'label' => __( 'Years', 'unite-child' ),
        'labels' => $labels,
        'public' => true,
        'hierarchical' => false,
        'label' => 'Years',
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'year', 'with_front' => true, ),
        'show_admin_column' => false,
        'show_in_rest' => false,
        'rest_base' => 'year',
        'show_in_quick_edit' => false,
    );
    register_taxonomy( 'year', array( 'films' ), $args );
}

add_action( 'init', 'unite_child_register_actors_taxonomy' );

/**
 * A new actors taxonomy to films post type with help of Custom Post Type UI
 * plugin. (https://wordpress.org/plugins/custom-post-type-ui/)
 *
 * @return void
 */
function unite_child_register_actors_taxonomy() {

    /**
     * Taxonomy: Actors.
     */
    $labels = array(
        'name' => __( 'Actors', 'unite-child' ),
        'singular_name' => __( 'Actor', 'unite-child' ),
        'menu_name' => __( 'Actor', 'unite-child' ),
        'all_items' => __( 'Actors', 'unite-child' ),
        'edit_item' => __( 'Edit Actor', 'unite-child' ),
        'view_item' => __( 'View Actor', 'unite-child' ),
        'update_item' => __( 'Update Actor Name', 'unite-child' ),
        'add_new_item' => __( 'Add New Actor', 'unite-child' ),
        'new_item_name' => __( 'New Actor Name', 'unite-child' ),
        'parent_item' => __( 'Parent Actor', 'unite-child' ),
        'parent_item_colon' => __( 'Parent Actor:', 'unite-child' ),
        'search_items' => __( 'Search Actors', 'unite-child' ),
        'popular_items' => __( 'Popular Actor', 'unite-child' ),
        'separate_items_with_commas' => __( 'Separate Actors with Commas', 'unite-child' ),
        'add_or_remove_items' => __( 'Add or remove Actors', 'unite-child' ),
        'choose_from_most_used' => __( 'Choose from most used Actors', 'unite-child' ),
        'not_found' => __( 'No Actors Found', 'unite-child' ),
        'no_terms' => __( 'No actors', 'unite-child' ),
        'items_list_navigation' => __( 'Actors list navigation', 'unite-child' ),
        'items_list' => __( 'Actors list', 'unite-child' ),
    );

    $args = array(
        'label' => __( 'Actors', 'unite-child' ),
        'labels' => $labels,
        'public' => true,
        'hierarchical' => false,
        'label' => 'Actors',
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'actor', 'with_front' => true, ),
        'show_admin_column' => false,
        'show_in_rest' => false,
        'rest_base' => 'actor',
        'show_in_quick_edit' => false,
    );
    register_taxonomy( 'actor', array( 'films' ), $args );
}
