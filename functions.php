<?php

/**
 * Custom functions that act independently of the theme templates.
 */
require get_stylesheet_directory() . '/inc/extras.php';

/**
 * Register new films custom post type with 2 extra fields.
 */
require get_stylesheet_directory() . '/inc/films-post-type.php';

/**
 * Register 4 taxonomies on films post type.
 */
require get_stylesheet_directory() . '/inc/films-taxonomies.php';

/**
 * Helper functions to format films fields.
 */
require get_stylesheet_directory() . '/inc/films-helpers.php';


add_action( 'wp_enqueue_scripts', 'unite_child_enqueue_parent_styles' );

/**
 * Add parent style.
 *
 * @return void
 */
function unite_child_enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

add_filter('the_content', 'unite_child_modify_content');

/**
 * Manipulate post object of films post type.
 *
 * @param  WP_Post $post
 * @return void
 */
function unite_child_modify_content($content) {
    global $post;
    if ($post->post_type == 'films') {
        $modified_content = $content;
        $modified_content .= '<ul>' . get_the_term_list( $post->ID, 'country', '<li class="genres_item">', ', ', '</li>' ) . '</ul>';
        $modified_content .= '<ul>' . get_the_term_list( $post->ID, 'genre', '<li class="genres_item">', ', ', '</li>' ) . '</ul>';

        $modified_content .= '<p>' . __('Price: ', 'unite-child') . unite_child_get_films_price($post->ID) . '</p>';
        $modified_content .= '<p>' . __('Release date: ', 'unite-child') . unite_child_get_films_release_date($post->ID) . '</p>';

        return $modified_content;
    } else {
        return $content;
    }
}

add_shortcode('films', 'unite_child_adsense');

/**
 * Shortcode to list last 5 films.
 *
 * @param  array $atts
 * @return string
 */
function unite_child_adsense( $atts ) {
    $args = array(
        'post_type'   => 'films',
        'post_status' => 'publish',
    );

    $films = new WP_Query( $args );
    if( $films->have_posts() ) : ?>
        <ul>
        <?php while( $films->have_posts() ) : $films->the_post(); ?>
            <li>
                <a href="<?php echo get_permalink(); ?>"><h3><?php echo get_the_title(); ?></h3></a>
                <p><strong><?php echo unite_child_get_films_price(get_the_ID()); ?></strong></p>
                <p><em><?php echo unite_child_get_films_release_date(get_the_ID()); ?></em></p>
        <?php endwhile; wp_reset_postdata(); ?>
        </ul>
    <?php
    else :
        _e( 'No films yet!', 'unite-child' );
    endif;
}


